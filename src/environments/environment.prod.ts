export const environment = {
  production: true,

  authenticateUrl: 'https://backend.ithubbs.de/api/authenticate',

  adminUrl: 'https://backend.ithubbs.de/api/admin',
  adminEventsUrl: 'https://backend.ithubbs.de/api/admin/events',
  adminGroupsUrl: 'https://backend.ithubbs.de/api/admin/groups',
  groupsUrl: 'https://backend.ithubbs.de/api/groups',

  eventsUrl: 'https://backend.ithubbs.de/api/events'
};

import {Component, OnInit} from '@angular/core';
import {EventStatistics} from '../shared/EventStatistics';
import {EventService} from '../shared/event.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  eventStatistics: EventStatistics;

  constructor(private eventService: EventService) { }

  ngOnInit() {
    this.eventService.getEventStatistics().subscribe(eventStatistics => {
      this.eventStatistics = eventStatistics;
    });
  }

}

import {Component, OnInit} from '@angular/core';
import {EventStatistics} from '../shared/EventStatistics';
import {EventService} from '../shared/event.service';

@Component({
  selector: 'app-current-everything',
  templateUrl: './current-everything.component.html',
  styleUrls: ['current-everything.component.css']
})
export class CurrentEverythingComponent implements OnInit {

  eventStatistics: EventStatistics;

  constructor(private eventService: EventService) { }

  ngOnInit() {
    this.eventService.getEventStatistics().subscribe(eventStatistics => {
      this.eventStatistics = eventStatistics;
    });
  }

}

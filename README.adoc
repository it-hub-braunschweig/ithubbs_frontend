= IT-Hub Braunschweig Frontend

This is the frontend to http://www.ithubbs.de[ithubbs.de]

== Run locally

1. This is an Angular web application. To be able to run it, you have to setup your local work environment first, see https://angular.io/guide/setup-local

2. With NPM and the Angular CLI set up, build the project by running

	npm install

3. Now you may run the application with

	ng serve

4. The frontend should be available at localhost:4200. If it is not, check the log for errors.

5. The frontend alone will only provide static content. To create groups and admins, you also have to setup the backend. If you followed the instructions at https://gitlab.com/it-hub-braunschweig/ithubbs_backend backend, you can login at localhost:4200/admin/login  with the credentials "steven" / "steven"
